# APIChallengeN5

## IMPORTANTE PARA PUBLICAR
### ERROR HTTP
### 1. Ejecutar CMD como Administrador
### 2. net stop http
### 3. net start http
      

## EJECUTAR SCRIPT EN SQL SERVER
### `script_db.sql`
### INICIO SCRIPT
CREATE DATABASE challengen5;
GO
USE challengen5;
GO
CREATE TABLE Permisos (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(150) NOT NULL,
    ApellidoEmpleado VARCHAR(150) NOT NULL,
    TipoPermiso INT NOT NULL,
	FechaPermiso DATE NOT NULL
);
GO
CREATE TABLE TipoPermisos (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Descripcion VARCHAR(150) NOT NULL
);
GO
ALTER TABLE Permisos
   ADD CONSTRAINT FK_Permisos_TipoPermisos FOREIGN KEY (TipoPermiso)
      REFERENCES TipoPermisos (Id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
;
### FIN SCRIPT



## CONFIGURAR CADENA DE CONEXION -> APIChallengeN5/appsettings.json
{
  "ConnectionStrings": {
    "DB_CN5": "<cadena_de_conexion>"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
