﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIChallengeN5.Entities
{
    public class TipoPermiso
    {
        [Key]
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
