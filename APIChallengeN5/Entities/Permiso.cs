﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIChallengeN5.Entities
{
    [Serializable]
    public class Permiso
    {
        [Key]
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidoEmpleado { get; set; }

        public int TipoPermiso { get; set; }
        public DateTime FechaPermiso { get; set; }
        public virtual string StrFechaPermiso { 
            get {
                return FechaPermiso.ToString("yyyy-MM-dd");
            } 
        }
    }
}
