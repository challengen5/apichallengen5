﻿using APIChallengeN5.Context;
using APIChallengeN5.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIChallengeN5.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPermisosController : ControllerBase
    {
        private readonly AppDBContext context;

        public TipoPermisosController(AppDBContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoPermiso>>> Get()
        {
            return await context.TipoPermisos.ToListAsync();
        }
    }
}
