﻿using APIChallengeN5.Context;
using APIChallengeN5.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIChallengeN5.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermisosController : ControllerBase
    {
        //Incluido el constructor el injeccion de dependencias
        private readonly AppDBContext context;

        public PermisosController(AppDBContext context) {
            this.context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Permiso>>> Get()
        {
            return await context.Permisos.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Permiso>> Get(int id)
        {
            var permiso = await context.Permisos.FindAsync(id);
            if (permiso == null)
            {
                return NotFound();
            }
            return permiso;
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Permiso permiso) {
            if (id != permiso.Id) {
                return BadRequest();
            }

            context.Entry(permiso).State = EntityState.Modified;
            try {
                await context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                //if(!PermisoExists)
                return NotFound();
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Permiso permiso) {
            context.Permisos.Add(permiso);
            await context.SaveChangesAsync();
            return CreatedAtAction("Get", new { id = permiso.Id }, permiso);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id) {
            var permiso = await context.Permisos.FindAsync(id);
            if (permiso == null) {
                return NotFound();
            }

            context.Permisos.Remove(permiso);
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
