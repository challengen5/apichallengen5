﻿using APIChallengeN5.Controllers;
using APIChallengeN5.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace APIChallengeN5.Tests
{
    [TestClass]
    public class PermisosControllerTest: BaseTests
    {
        [Fact]
        public async Task GetPermisions_Validate()
        {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Maria", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 1});
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Angel", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 2});
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Carlos", ApellidoEmpleado = "Zapata", FechaPermiso = DateTime.Now, TipoPermiso = 2});
            await context.SaveChangesAsync();

            var context2 = BuildContext(nameDB);

            //Prueba
            var controller = new PermisosController(context2);
            var response = await controller.Get();

            //Verificacion
            var permissions = response.Value;
            Assert.AreEqual(3, permissions.Count());
        }

        [TestMethod]
        public async Task GetPermisions_Invalidate() {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Maria", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 1 });
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Angel", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 2 });
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Carlos", ApellidoEmpleado = "Zapata", FechaPermiso = DateTime.Now, TipoPermiso = 2 });
            await context.SaveChangesAsync();

            var context2 = BuildContext(nameDB);

            //Prueba
            var controller = new PermisosController(context2);
            var response = await controller.Get();

            //Verificacion
            var permissions = response.Value;
            Assert.AreEqual(2, permissions.Count());
        }

        [TestMethod]
        public async Task GetPermisionById_Validate()
        {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);

            //Prueba
            var controller = new PermisosController(context);
            var response = await controller.Get(1);

            //Verificacion
            var status = response.Result as StatusCodeResult;
            Assert.AreEqual(200, status.StatusCode);
        }

        [TestMethod]
        public async Task GetPermisionById_InValidate()
        {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);

            //Prueba
            var controller = new PermisosController(context);
            var response = await controller.Get(1);

            //Verificacion
            var status = response.Result as StatusCodeResult;
            Assert.AreEqual(404, status.StatusCode);
        }

        [TestMethod]
        public async Task RequestPermision_Validate()
        {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);

            var newPermission = new Permiso() { Id = 1, NombreEmpleado = "Maria", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 1 };

            //Prueba
            var controller = new PermisosController(context);
            var response = await controller.Post(newPermission);

            //Verificacion
            var result = response as CreatedAtRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task ModifyPermision_Validate()
        {
            //Preparacion
            var nameDB = Guid.NewGuid().ToString();
            var context = BuildContext(nameDB);
            
            context.Permisos.Add(new Permiso() { Id = 1, NombreEmpleado = "Maria", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 1 });
            await context.SaveChangesAsync();

            var context2 = BuildContext(nameDB);

            var updatePermission = new Permiso() { Id = 1, NombreEmpleado = "Maria Upd", ApellidoEmpleado = "Ramirez", FechaPermiso = DateTime.Now, TipoPermiso = 1 };

            //Prueba
            var controller = new PermisosController(context2);

            var id = 1;
            var response = await controller.Put(id, updatePermission);

            //Verificacion
            var result = response as StatusCodeResult;
            Assert.AreEqual(200, result.StatusCode);

            var context3 = BuildContext(nameDB);
            var permission = context3.Permisos.FirstOrDefault(x => x.NombreEmpleado == "Maria Upd");
            Assert.IsNotNull(permission);
        }
    }
}
