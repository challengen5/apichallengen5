﻿using APIChallengeN5.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIChallengeN5.Tests
{
    public class BaseTests
    {
        protected static AppDBContext BuildContext(string nameDB) {
            var options = new DbContextOptionsBuilder<AppDBContext>()
                .UseInMemoryDatabase(nameDB).Options;

            var dbContext = new AppDBContext(options);
            return dbContext;
        }
    }
}
